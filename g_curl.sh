#!/usr/bin/env bash

$pid

hakrawler -url $1 -plain -depth 3 | tee -a hkr_$url.txt

wait $pid

sed 's/^/url=/;' hkr_$url.txt | tee -a curls.txt

wait $pid

curl -sS -K curls.txt | grep -f regexapi.txt > g_curl_results

