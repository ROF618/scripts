#!/usr/bin/env bash

xargs -n 1 curl $line --proxy 127.0.0.1:8080 --cacert /home/chris/Utils/BurpSuiteCommunity/PortSwiggerCA.crt < $1
