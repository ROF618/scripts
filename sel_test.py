#!/usr/bin/python3

from selenium import webdriver

try:
    fireFoxOptions = webdriver.FirefoxOptions()
    fireFoxOptions.set_headless()
    brower = webdriver.Firefox(firefox_options=fireFoxOptions)

    brower.get('https://pythonbasics.org')
    print(brower.page_source)

finally:
    try:
        brower.close()
    except:
        pass
