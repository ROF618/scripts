#!usr/bin/env python

import json
import sys
from datetime import date

today = date.today()

with open(str(sys.argv[1]), 'r') as f:
    data = json.load(f)

bug_bounty_programs = open(str(today) + '_results.txt', 'a+')

bug_bounty_programs.write(data[sys.argv[2]])
