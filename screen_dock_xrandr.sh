#!/usr/bin/bash
# write conditional that checks monitor script in auto_start.sh and excutes xrandr commands bassed on the aforementioned script's output. Also bind this script to a set of keys in /qtile/config.py
# Function to check if a monitor is connected
is_connected() {
    xrandr | grep "$1 connected"
}

# Function to execute xrandr commands based on monitor configuration
configure_monitors() {
    if is_connected "DisplayPort-2 connected"; then
        # Configuration when HDMI-1 is connected
	xrandr --output DisplayPort-2 --primary --left-of DisplayPort-0 --output eDP --off && qtile cmd-obj -o cmd -f reload_config
    else
        # Default configuration when no special configuration is detected
        echo "no need to change"
    fi
}

# Main function
main() {
    echo "Detecting connected monitors..."
    configure_monitors
    echo "Configuration set."
}

# Run the main function
main
