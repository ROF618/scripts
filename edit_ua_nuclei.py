#!/usr/bin/evn python3

import yaml
import fileinput
import sys

#file_names = sys.stdin.read()

for line in fileinput.input():
    #print(type(line))
    with open(line.rstrip(), 'r') as stream:
        try:
            values_yaml = yaml.safe_load(stream)
            # below needs to be set to a value e.g. mozilla firefox ubuntu linux
            if values_yaml['requests'][0].get('headers') != None:
                print(values_yaml['requests'][0].get('headers'))# = 'Mozilla/5.0 (X11; Linux x86_64; rv:87.0) Gecko/20100101 Firefox/87.0'
            with open(line.rstrip(), 'w') as f:
                yaml.dump(values_yaml,f)
        except yaml.YAMLError as exc:
            print(exc)
