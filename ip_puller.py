#/usr/bin/evn python3

import re
import sys

out_file = open('raw_ips_'+ str(sys.argv[1]), 'w+')
valid = set()


with open(str(sys.argv[1])) as f_input:
    for ip in re.findall(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})', f_input.read()):
        try:
            valid.add(ip)
        except socket.error:
            pass

for i in valid:
    out_file.write("%s\n" % str(i))

