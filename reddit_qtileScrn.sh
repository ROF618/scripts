#!/bin/bash
# found here: https://www.reddit.com/r/qtile/comments/15zrgrs/monitor_hotplugging_while_using_qtile/

export PATH=/usr/bin

if [[ $(</sys/class/drm/card1-HDMI-A-1/status) == "connected" ]]
then
	pactl set-card-profile alsa_card.pci-0000_00_1f.3 output:hdmi-stereo
	xrandr --output HDMI-1 --auto && xrandr --output eDP-1 --off
else
	pactl set-card-profile alsa_card.pci-0000_00_1f.3 output:analog-stereo
	xrandr --output eDP-1 --auto && xrandr --output HDMI-1 --off
fi

config="$AUTORANDR_CURRENT_PROFILE"
notify-send "Switched to config $AUTORANDR_CURRENT_PROFILE"
killall -USR1 qtile
nitrogen --restore &
