#!/usr/bin/env bash

# most scripts are pulled from here > https://gist.github.com/m4ll0k/31ce0505270e0a022410a50c8b6311ff
pid=$!

if [[ "$1" != "" ]]; then
	url="$1"
	echo $url
else
	read -p "Enter url to pull js files " url
	echo $url
fi

gau $url | grep -iE '\.js' | grep -ivE '\.json' | sort -u >> $url.txt
echo "gau finished"

wait $pid

cat $url.txt | xargs -n2 -I@ bash -c "echo -e '\n[URL]: @\n'; python3 ~/Tools/LinkFinder/linkfinder.py -i @ -o cli" >> js_path_$url.txt

echo "finished linkfinder"
wait $pid

cat js_path_$url.txt | grep -iv '[URL]:'|| sort -u >> js_n_path_$url.txt

echo "finished greping"
wait $pid

#cat $url_js_path_nURL.txt | python3 ~/Tools/m4ll0k_scripts/collector.py output

#echo "finished collector"

## Need to add secretFinder but make sure to go through script

cat js_path_$url.txt | xargs -I @ bash -c 'python3 ~/Tools/LinkFinder/linkfinder.py -i @ -o cli' | python3 ~/Tools/scripts/m4ll0k_scripts/collector.py output_$url

echo "finished collector"
wait $pid

cat output_$url/urls.txt | python3 ~/Tools/scripts/m4ll0k_scripts/availableForPurchase.py

