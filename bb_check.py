#!/usr/bin/env python3
import sys
import json

with open(str(sys.argv[1]), 'r') as f:
    data = json.load(f)

raw_probed_hosts = open('raw_probed_' + str(sys.argv), 'a+')

for item in data:
    raw_probed_hosts.write(str(item['url']))
    raw_probed_hosts.write('\n')
