#!/usr/bin/env bash


## run apt update && apt -d -y upgrade before running this script


apt install hostapd


pid=$!
wait $pid

DEBIAN_FRONTEND=noninteractive apt install -y netfilter-persistent iptables-persistent

wait $pid

systemctl unmask hostapd
wait $pid
systemctl enable hostapd

wait $pid

apt install dnsmasq

wait $pid

rm /etc/dhcpcd.conf
cp ./dhcpcd.conf /etc/

wait $pid

rm /etc/sysctl.d/hostapd.conf
cp ./routed-ap.conf /etc/sysctl.d/

wait $pid

iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE

wait $pid

netfilter-persistent save

wait $pid

rm /etc/dnsmasq.conf
cp ./dnsmasq.conf /etc/dnsmasq.conf

wait $pid

rm /etc/hostapd/hostapd.conf
cp ./hostapd.conf /etc/hostapd/hostapd.conf
