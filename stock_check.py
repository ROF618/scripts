#!/usr/bin/python3

import json
import requests

rsp = requests.get('https://finance.google.com/finance?q=AAPL&output=json')

if rsp.status_code in (200,):
    fin_data = json.loads(rsp.content[6:-2].decode('unicode_escape'))

    print(fin_data(['op']))
    print(fin_data(['pe']))
