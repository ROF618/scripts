#!/usr/bin/env bash

read -p "enter path to a directory (remember to end in /*) " dir
read -p "enter the name of output file " output

for f in $dir
do
	tshark -r $f -Y http.request -T fields -e http.host -e http.user_agent -e http.cookie >> $output.txt
done
