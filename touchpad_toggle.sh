#!/usr/bin/env bash

if xinput list-props 12 | grep "Device Enabled" | awk '{print $4}'
